package com.love.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootNettyUdpApplication {
	public static void main(String[] args) {
		SpringApplication.run(SpringBootNettyUdpApplication.class,args);
	}
}
