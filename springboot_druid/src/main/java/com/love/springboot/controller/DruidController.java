package com.love.springboot.controller;

import com.love.springboot.config.DruidConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author aishiyushijiepingxing
 * @description
 * @date 2020/5/18
 */
@RestController
public class DruidController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DruidController.class);

    @Resource
    private JdbcTemplate jdbcTemplate;

    @RequestMapping("/getDruidData")
    public String druidData() {
        String sql = "select count(1) from page";
        Integer countOne = jdbcTemplate.queryForObject(sql, Integer.class);
        LOGGER.info("countOne：" + countOne);
        return "getDruidData";
    }
}
