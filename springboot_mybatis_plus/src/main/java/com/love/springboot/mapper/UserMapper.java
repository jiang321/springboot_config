package com.love.springboot.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.love.springboot.model.User;

public interface UserMapper extends BaseMapper<User> {

}